from random import choice


class Grammar:
    def __init__(self):
        self.rules = {}

    def add_rule(self, symbol, rule):
        if not isinstance(rule, str):
            rule = rule(self.rules)
        rule = [x.split() for x in rule.split('|')]
        self.rules[symbol] = rule

    def adverbify(self, symbol, rule):
        self.rules[symbol] = [[f'{e}ly'] for r in self.rules[rule] for e in r]

    def generate(self, start='S'):
        result = []
        if start not in self.rules:
            return start

        selected = choice(self.rules[start])
        for symbol in selected:
            result.append(self.generate(symbol))

        return ' '.join(result)


g = Grammar()

# Rules
g.add_rule('S', 'PM')
g.add_rule('PM', 'PB | PB and PA')  # its called PM bc i run out of name ideas.
g.add_rule('PB', 'PV1 PA | PV1 PA T')  # base phrase - (pronoun + verb) + (adj phrase) + optional time
g.add_rule('PA', 'Adj | Adv Adj | Comp')  # adjective phrase
g.add_rule('Comp', 'as Adj as DP | more Adj than DP')  # comparative phrase
g.add_rule('PV1', 'PP V1')  # verb phrase 1
g.add_rule('PV2', 'PP V2')  # verb phrase 1
g.add_rule('T', 'when PV2 | every day | all the time')  # time


# Vocabulary
# g.add_rule('J', 'Jess | Jessica | she')  # pronouns/nouns
g.add_rule('PP', 'you | u')  # personal pronouns (plural)

g.add_rule('DP', 'a N | the NN | an NNN')  # determinant phrase
g.add_rule('N', 'flower | rainbow | butterfly | peacock | kitten | puppy | cherry blossom | diamond | emerald | '
                'sapphire | pearl | ruby | waterfall | ocean wave | snowflake | mountain peak | rose | daisy | tulip | '
                'hummingbird | ladybug | snow-capped peak | thunderstorm | tranquil river | shimmering lake | galaxy | '
                'majestic waterfall')  # nouns (countable)
g.add_rule('NN', 'sky | sun | night | sunrise | sunset | starry sky | city at midnight | moon | autumn leaves | '
                 'universe')  # nouns (uncountable)
g.add_rule('NNN', 'emerald green jungle | endless prairie ')  # nouns (countable, starting w vowel)


# verbs
g.add_rule('V1', 'are | r | look')  # verbs 1
g.add_rule('V2', 'smile | talk | sleep | laugh')  # singular verbs 2

# adjectives
g.add_rule('Adj', 'pretty | cute | fabulous | amazing | adorable | beautiful | dazzling | elegant | '
                  'fascinating | gorgeous | graceful | lovely | marvelous | splendid | stunning | superb | wonderful')
# just take all adjectives and turn them into adverbs adding 'ly'. will fail sometimes but oh well
g.adverbify('Adv', 'Adj')

# for export
compliment_grammar = g
