import {Button, Container, List, MantineProvider, Center, Text, Title, Card, Footer, Badge} from '@mantine/core';
import {useEffect, useRef, useState} from 'react'
import {FancySlider} from "./FancySlider.jsx";

async function get_compliment(n = 1, limit = 1) {
    const response = await fetch(
        `/api/compliment?n=${n}&limit=${limit}`,
        {
            method: 'GET'
        })
    return await response.json();
}

function gradient() {
    const hexVals = '0123456789abcdef';

    function createHex() {
        let code = '#'
        for (let i = 0; i < 6; i++) {
            code += hexVals.charAt(Math.floor(Math.random() * hexVals.length));
        }
        return code
    }

    return {
        deg: Math.floor(Math.random() * 360),
        from: createHex(),
        to: createHex()
    }
}


function App() {
    const [compliments, setCompliments] = useState([])
    const colors = compliments.map(gradient)
    const [n, setN] = useState(5)
    const [limit, setLimit] = useState(1)

    const buttonRef = useRef(null);

    useEffect(() => {
        buttonRef.current.click();
    }, []);

    return (
        <MantineProvider theme={{colorScheme: 'dark'}} withGlobalStyles withNormalizeCSS>
            <Container maw={800} mx="auto">

                <Title mt="xl" mb="xl" align="center">Hey there cute!!</Title>
                <Container mt="md" maw={400} mx="auto">
                    <Text mb="xl">
                        Made this thinking of u, hope u like it
                    </Text>
                    <FancySlider value={n} onChange={setN} label="Number of sentences"/>
                    <FancySlider value={limit} onChange={setLimit} label="Min. words per sentence"/>
                </Container>

                <Center mt="xl">
                    <Button mt="xl"
                            variant="gradient"
                            gradient={{from: '#ed6ea0', to: '#ec8c69', deg: 35}}
                            size="xl"
                            onClick={async () => setCompliments(await get_compliment(n, limit))}
                            ref={buttonRef}
                    >
                        Generate
                    </Button>
                </Center>

                <Card mt="xl">
                    <List>{compliments.map((sentence, idx) =>
                        <Text
                            key={idx}
                            variant="gradient"
                            gradient={colors[idx]}
                        >
                            {sentence}
                        </Text>)}</List>
                </Card>

                <Footer height={60}>me, 2023
                    <a href="https://gitlab.com/itsame_mario/compliment-generator" target="_blank">
                        <Badge variant="dark">Da Code</Badge>
                    </a>
                </Footer>
            </Container>
        </MantineProvider>
    )
}

export default App
