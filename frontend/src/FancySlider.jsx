import React from 'react'
import {Container, rem, Slider, Text} from "@mantine/core";
import {IconHeart} from "@tabler/icons-react";

export function FancySlider(props) {
    return (
        <Container>
            <Text>{props.label}</Text>
            <Slider mt="xs" mb="md" min={1} max={20} step={1}
                    color="red" thumbSize={26}
                    styles={{thumb: {borderWidth: rem(2), padding: rem(3)}}}
                    value={props.value} onChange={props.onChange}
                    thumbChildren={<IconHeart size="1rem"/>}
            />
        </Container>
    )
}