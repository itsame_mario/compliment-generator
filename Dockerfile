FROM node:16-alpine as builder

WORKDIR /app

# install dependencies
COPY ./frontend/package*.json ./
RUN npm install

COPY ./frontend .
RUN npm run build

# serve the built files with fastapi
FROM python:3.9-alpine as final

WORKDIR /app

COPY main.py requirements.txt ./

RUN pip install -r requirements.txt --no-cache-dir

COPY --from=builder /app/dist /app/frontend/dist

COPY ./api ./api

EXPOSE 8000

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]

