from fastapi import FastAPI
from fastapi.requests import Request
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles

from api.grammar import compliment_grammar

app = FastAPI()

app.mount('/static', StaticFiles(directory='frontend/dist', html=True), name='frontend')


@app.middleware('http')
async def redirect_to_static(request: Request, call_next):
    if not request.url.path.startswith('/api') and not request.url.path.startswith('/static'):
        return RedirectResponse(url=f'/static{request.url.path}')
    return await call_next(request)


@app.get('/')
async def main():
    return RedirectResponse('/static')


@app.get('/api/compliment')
async def compliment(n: int = 1, limit: int = 1):
    compliments = []
    tries = 0
    while len(compliments) < n or tries > 10_000:
        c = compliment_grammar.generate()
        if len(c.split()) > limit:
            compliments.append(c)
        tries += 1

    return compliments
